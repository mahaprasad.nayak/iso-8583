module cbs_server

go 1.16

require (
	github.com/moov-io/iso8583 v0.15.3
	moul.io/banner v1.0.1 // indirect
)
